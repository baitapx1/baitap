<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<meta charset="utf-8">
	<meta name="view-port" content="width=device-width,initial-scale=1">
	<link rel="stylesheet" type="text/css" href="public/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="public/css/style.css">
	<script src="public/js/jquery-3.1.1.min.js"></script>
</head>
<body>
<div class="container-fluid" style="padding: 0">
	<div class="container-fluid header" style="padding: 0">
		<ul class="nav navbar-nav">
			<li><a href="index.php">HOME</a></li>
			<li><a href="index.php?controller=product">Product</a></li>
			<li><a href="index.php?controller=brand">Brand</a></li>
		</ul>
	</div>
	<div class="container-fluid content">
		<?php
			if (file_exists($controller)) include $controller;
			else echo "Welcome to my shop!";
		?>
	</div>
</div>
</body>
</html>