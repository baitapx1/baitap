<link rel="stylesheet" type="text/css" href="public/css/style.css">
<div class="col-md-10 col-md-offset-1">
	<div class="panel panel-success">
		 <div class="panel-heading">Product</div>
		 <div class="panel-body">
		 	<table class="table table-hover table-bordered">
		 		<tr>
		 			<th class="text-center">id_product</th>
		 			<th class="text-center">id_brand</th>
		 			<th class="text-center">category</th>
		 			<th class="text-center">name</th>
		 			<th class="text-center">function</th>
		 			<th class="text-center">price</th>
		 			<th class="text-center">image</th>
		 			<th class="text-center">made_in</th>
		 			<th class="text-center">weight</th>
		 			<th class="text-center">quantity</th>
		 			<th class="text-center">sold_quantity</th>
		 			<th style="width: 100px"></th>
		 		</tr>
		 		<?php
		 			foreach ($arr as $rows) {
		 				?>
		 				<tr>
		 					<td style="text-align: center"><?php echo $rows["id_product"];?></td>
		 					<td style="text-align: center"><?php echo $rows["id_brand"];?></td>
		 					<td style="text-align: center"><?php echo $rows["category"];?></td>
		 					<td style="text-align: center"><?php echo $rows["name"];?></td>
		 					<td style="text-align: center"><?php echo $rows["function"];?></td>
		 					<td style="text-align: center"><?php echo $rows["price"];?></td>
		 					<td><div style="height: 100px;width: 100px;margin: 0 auto"><img src=<?php echo $rows["link_img"];?> style="width: 100%;height: 100%"></div></td>
		 					<td style="text-align: center"><?php echo $rows["made_in"];?></td>
		 					<td style="text-align: center"><?php echo $rows["weight"];?></td>
		 					<td style="text-align: center"><?php echo $rows["quantity"];?></td>
		 					<td style="text-align: center"><?php echo $rows["sold_quantity"];?></td>
		 					<td style="text-align: center;">
		 						<a href="model/edit.php?id=<?php echo $rows["id_product"]?>" class="table-link">Edit</a><br>
		 						<a href="model/delete.php?id=<?php echo $rows["id_product"]?>&&tag=product" class="table-link">Delete</a>
		 					</td>
		 				</tr>
		 				<?php 
		 			}
		 		?>
		 	</table>
		 </div>
	</div>
</div>