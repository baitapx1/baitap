<link rel="stylesheet" type="text/css" href="public/css/style.css">
<div class="col-md-10 col-md-offset-1">
	<div class="panel panel-primary">
		 <div class="panel-heading">Brand</div>
		 <div class="panel-body">
		 	<table class="table table-hover table-bordered">
		 		<tr>
		 			<th class="text-center">id</th>
		 			<th class="text-center">name</th>
		 			<th class="text-center">image</th>
		 			<th style="width: 100px"></th>
		 		</tr>
		 		<?php
		 			foreach ($arr as $rows) {
		 				?>
		 				<tr>
		 					<td style="text-align: center"><?php echo $rows["id_brand"];?></td>
		 					<td style="text-align: center"><?php echo $rows["name"];?></td>
		 					<td style="text-align: center"><div style="height: 100px;width: 100px;margin: 0 auto"><img src=<?php echo $rows["link_img"];?> style="width: 100%;height: 100%"></div></td>
		 					<td style="text-align: center;">
		 						<a href="model/edit2.php?id=<?php echo $rows["id_brand"]?>" class="table-link">Edit</a><br>
		 						<a href="model/delete.php?id=<?php echo $rows["id_brand"]?>&&tag=brand" class="table-link">Delete</a>
		 					</td>
		 				</tr>
		 				<?php 
		 			}
		 		?>
		 	</table>
		 </div>
	</div>
</div>