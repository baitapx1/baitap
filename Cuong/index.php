<meta charset="utf-8">
<?php
	//bat dau session
	session_start();
	//them vao config ket noi csdl
	include "config.php";
	//them vao model
	include "model/model.php";
	//them vao controller
	include "controller/controller.php";
	//dieu huong thong qua link
	$c = isset($_GET["controller"])?$_GET["controller"]:"";
	$controller = "controller/controller_home.php";

	if ($c != "") $controller = "controller/controller_$c.php";
	include "view/home.php";
?>