<?php
	class model{
		//ham truy van du lieu
		function query($sql){
			global $link;
			mysqli_query($link,$sql);
		}
		//ham lay toan bo du lieu
		function fetchAll($sql){
			global $link;
			$result = mysqli_query($link,$sql);
			$arr = array();
			while($row = mysqli_fetch_array($result)) $arr[] = $row;
			return $arr;
		}
		//ham lay 1 hang trong bang
		function fetchOne($sql){
			global $link;
			$result = mysqli_query($link,$sql);
			$arr = mysqli_fetch_array($result);
			return $arr;
		}
	}
?>