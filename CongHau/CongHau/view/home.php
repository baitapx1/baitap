<!DOCTYPE html>
<html>
<head>
	<title>Boshop.com</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="public/css/pro.css">
	<link rel="stylesheet" href="public/font-awesome/css/font-awesome.min.css">
	<!-- Latest compiled and minified CSS & JS -->
	<link rel="stylesheet" href="public/bootstrap/css/bootstrap.min.css">
	<script src="public/bootstrap/jquery.js"></script>
	<script src="public/bootstrap/js/bootstrap.min.js"></script>
	<style>
		.fa{
			color: green;
		}
		.proContent > p{
			width: 90%;

		    text-align: justify;
		    margin: 15px auto;
		}
		.origin{
			margin-left: 120px;
		}
		.price{
			font-size: 25px;
			font-weight: 300;
			color: red;
			margin-left: 30%;
		}
		form{
			margin-top: 15px;
			margin-left: 20%;
		}
		a{
			font-size: 22px;
			margin-top: 30px;
			display: inline-block;
		}
		.product{
			width: 80%;
			overflow: auto;
			margin: 15px auto;
			border: 1px solid red;
			border-radius: 10px;
		}
	</style>
</head>
<body>

	<div class="container">
		<?php 
		
			foreach ($row as $k) {
			
		?>

		<div class="product">
			<div class="proImage">

				<?php 
					echo "<img src='public/".$k['link_img']."'>";
				 ?>
				
			</div>
			<div class="proContent">
				<?php 
					echo "<h2>".$k['name']."</h2>";
				 ?>
				<?php echo "<span><i class='fa fa-check'></i> Mã sản phẩm : ".$k['id_product']."</span>";?>
				<?php echo "<span class = 'origin'><i class='fa fa-check'></i> Xuất xứ : ".$k['made_in']."</span>"; ?>
				<?php echo "<p>".$k['function']."</p>" ?>
				<?php echo "<span class ='price'> Giá bán: ".$k['price']."đ</span>"; ?>
				<br>
				<a href=<?php echo "?edit=".$k['id_product']."&link=".$k['link_img']; ?> class= "btn btn-primary">EDIT</a>
				<a href=<?php echo "?delete=".$k['id_product']; ?> class= "btn btn-primary">DELETE</a>
			</div>
		</div>

	 	<?php 
	 		}
	 	?>
	</div>
	
</body>
</html>