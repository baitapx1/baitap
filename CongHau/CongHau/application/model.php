<?php 

	/**
	* 
	*/
	class Model
	{
		public function getOne($sql){
			global $con;
			$result = mysqli_query($con,$sql);
			$row = mysqli_fetch_assoc($result);
			return $row;
		}
		public function getAll($sql){
			global $con;

			$result = mysqli_query($con,$sql);
			$arr = array();

			while ($row = mysqli_fetch_assoc($result)) {
				$arr[] = $row;
			}
			return $arr;
		}
		public function count($sql){
			global $con;
			$result = mysqli_query($con,$sql);
			return mysqli_num_rows($result);
		}
		public function run($sql){
			global $con;
			mysqli_query($con,$sql);

		}
	}
 ?>