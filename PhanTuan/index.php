<?php
	session_start();
	include 'config.php';
	include 'app/model.php';
	include 'app/controller.php';
	include 'app/pagination.php';

	$id = isset($_GET['id']) && filter_var($_GET['id'], FILTER_VALIDATE_INT) ? $_GET['id'] : 0;

	$controller = isset($_GET['controller']) ? $_GET['controller'] : "home";
	if(!file_exists("./controller/controller_$controller.php")) {
		$controller = "home";
	}
	include "controller/controller_$controller.php";
?>
