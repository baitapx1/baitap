<?php
	/**
	* 
	*/
	class controller_home extends controller
	{
		function checkInputSearch() {
			if(!isset($_GET['sub']) || !isset($_GET['text']) 
			   || !isset($_GET['option']) || $_GET['text'] == '')
			{
				return false;
			}
			$option = array("name", "brand", "category", "function", "made_in");
			$check = false;
			for ($i=0; $i < count($option); $i++) { 
				if($_GET['option'] == $option[$i])
					$check = true;
			}
			return $check;
		}
		function __construct()
		{
			parent::__construct();
			$search = "";
			$get = "";
			$split = array();
			if($this->checkInputSearch()) {
				if($_GET['option'] == "brand") {
					$option = "brand.name";
				} else {
					$option = "product.".$_GET['option'];
				}
				$search = " AND (";
				$text = $_GET['text'];
				$token = strtok($text, " .,");
				$split[] = $token;
				$getText = $token;
				$search .= " $option LIKE '%$token%' ";
				while (true) {
					$token = strtok(" .,");
					if($token == false) break;
					$split[] = $token;
					$getText .= "+".$token;
					$search .= " OR $option LIKE '%$token%' ";
				}
				$search .= ")";
				$get = "option=".$_GET['option']."&text=".$getText."&sub=Search&";
			}
			$sql = "SELECT product.*, brand.name as brand
					FROM product, brand 
					WHERE product.id_brand = brand.id_brand $search;";
			// echo "<br>".$sql."<br>";
			$arr = $this->model->selectAll($sql);
			
			$config = array(
				'current_page'  => isset($_GET['page']) && filter_var($_GET['page'], FILTER_VALIDATE_INT) ? $_GET['page'] : 1, 
			    'total_record'  => count($arr), 
			    'limit'         => 8,
			    'range'         => 5,
			    'link'			=> "index.php?".$get."page="
			);
			$pagination = new pagination($config);
			include 'view/home.php';
		}
		
	}
	$x = new controller_home();
		
?>