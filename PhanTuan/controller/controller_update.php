<?php

	class controller_update extends controller
	{
		
		function __construct()
		{
			parent::__construct();
			global $id;

			if(!isset($_POST['name'])) {
				$arr = $this->model->selectAll("SELECT * FROM brand;");
				$data = $this->model->selectOne("SELECT * FROM product WHERE id_product = $id;");
				if($data['id_product'] == '') {
					$id = 0;
					$_GET['id'] = 0;
				} 
				// echo "$id";
				include 'view/update.php';
			} else {
				$name = $_POST['name'];
				$id_brand = $_POST['brand'];
				$category = $_POST['category'];
				$function = $_POST['function'];
				$made_in = $_POST['made_in'];
				$weight = $_POST['weight'];
				$price = $_POST['price'];
				$quantity = $_POST['quantity'];
				$sold_quantity = $_POST['sold_quantity'];
				$link_img = "img/".$_FILES['link_img']['name'];
				move_uploaded_file($_FILES['link_img']['tmp_name'], "./public/".$link_img);

				if($id == 0) {
					$sql = "insert INTO product (id_brand, category, name, function, price, link_img, made_in, weight, quantity, sold_quantity) VALUE ($id_brand, '$category', '$name', '$function', $price, '$link_img', '$made_in', $weight, $quantity, $sold_quantity);";

					$this->model->execute($sql);
					$id = $this->model->getNewestId();
				} else {
					$sql = "
						UPDATE product 
						SET name = '$name', id_brand = $id_brand, price = $price, made_in = '$made_in', quantity = $quantity, weight = $weight, category = '$category', function = '$function', link_img = '$link_img',
							sold_quantity = $sold_quantity
						WHERE id_product = $id;";

					$this->model->execute($sql);
				}
				header("Location: index.php?controller=product&status=show&id=$id");
				
			}
		}
	}
	new controller_update();
?>