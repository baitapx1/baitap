<?php
	/**
	* 
	*/
	class controller_product extends controller
	{
		
		function __construct()
		{
			parent::__construct();

			global $id;
			$status = isset($_GET['status']) ? $_GET['status'] : '';

			if($status == 'show'){
				$sql = "SELECT product.*, brand.name as brand
						FROM product, brand 
						WHERE product.id_brand = brand.id_brand 
						AND id_product = '$id';";
				$arr = $this->model->selectOne($sql);
				
				include 'view/product.php';
			} else if($status == 'delete') {
				$sql = "DELETE FROM product WHERE id_product = $id;";
	   			$this->model->execute($sql);
	   			
	   			header("Location: index.php?controller=product&status=show&id=$id");
			} 
		}
	}
	new controller_product();
?>