<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">></script>
	<link rel="stylesheet" type="text/css" href="public/css/style3.css">
</head>
<body>
	<form action="index.php?controller=update&id=<?php echo $id ?>" 
	method="post" enctype="multipart/form-data">
		<h1>
			<?php 
				if($id == 0) echo "Insert a new product..."; 
				else echo "Edit a product...";
			?>
		</h1>
		<img src="public/<?php echo $data['link_img'] ?>">
		<table>
			<tr>
				<td>Tên:</td>
				<td><input type="text" name="name" value="<?php echo $data['name'] ?>" required ></td>
			</tr>
			<tr>
				<td>Thương hiệu:</td>
				<td>
					<select name="brand" value="<?php echo $data['name'] ?>">
						<?php
							for ($i=0; $i < count($arr); $i++) { 
								$obj = $arr[$i];
								$select = "";
								if($obj['id_brand'] == $data['id_brand']) {
									$select = "selected = 'selected' ";
								}
								?>
								<option <?php echo $select ?>
								value = "<?php echo $obj['id_brand'] ?>">
									<?php echo $obj['name'] ?>
								</option>
								<?php
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Loại sp:</td>
				<td>
					<select name="category">
						<?php  
							$category = array("Chăm Sóc Da","Trang Điểm","Chăm Sóc Cơ Thể", "Nước Hoa", "Mỹ Phẩm Cho Nam");
							for($i=0; $i < count($category); $i++) {
								$x = $category[$i];
								$select = "";
								if($data['category'] == $x) 
									$select = "selected='selected' " ;

								?>

								<option <?php echo $select ?>
								value = "<?php echo $x ?>">
									<?php echo $x ?>
								</option>

								<?php
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Đặc điểm:</td>
				<td><textarea rows="4" cols="50" name="function" required
				><?php echo $data['function'] ?></textarea></td>
			</tr>
			
			<tr>
				<td>Xuất xứ:</td>
				<td><input type="text" name="made_in" required
				value="<?php echo $data['made_in'] ?>"></td>
			</tr>
			<tr>
				<td>Ảnh:</td>
				<td><input type="file" name="link_img" required
				></td>
			</tr>
			<tr>
				<td>Giá:</td>
				<td><input type="text" name="price" class="number" required
				value="<?php echo $data['price'] ?>">đ</td>
			</tr>
			<tr>
				<td>Weight:</td>
				<td>
					<input type="text" name="weight" class="number" required
					value="<?php echo $data['weight'] ?>">g
				</td>
			</tr>
			<tr>
				<td>Số lượng:</td>
				<td><input type="text" name="quantity" class="number" required
				value="<?php echo $data['quantity'] ?>"></td>
			</tr>
			<tr>
				<td>Đã bán:</td>
				<td><input type="text" name="sold_quantity" class="number" required value="<?php echo $data['sold_quantity'] ?>"></td>
			</tr>
			<tr>
				<td>Update</td>
				<td><input type="submit" value="OK" id="update"></td>
			</tr>
		</table>	
	</form>
	<script type="text/javascript" src="public/js/js3.js"></script>
</body>
</html>