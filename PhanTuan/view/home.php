<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">></script>
	<link rel="stylesheet" type="text/css" href="public/css/style1.css">

</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<button id="home">Home</button>
			</div>
			<div class="col-md-3">
				<button id="add">ADD Product</button>
			</div>
			<div class="col-md-6">
				<form action="" method="get" class="form-inline">
					<select name="option">
						<option value="name">Name</option>
						<option value="category">Category</option>
						<option value="brand">Brand</option>
						<option value="function">Function</option>
						<option value="made_in">Made In</option>
					</select>
					<input type="text" name="text" class="form-control" placeholder="Search">
					<input type="submit" name="sub" value="Search" class="btn btn-default">
				</form>
			</div>
		</div>
	</div>
	<hr>
	<div class="container">
		<div class="row">
			<p>
				<?php
					$len = count($split);
					if($len == 0) {
						echo "Home";
					} else {
						$s = "Kết quả tìm kiếm cho <b>".$_GET['option']."</b>: ";
						for ($i=0; $i < $len; $i++) { 
							$s .= "'$split[$i]'";
							if($i < $len - 1) $s .= ", ";
						}	
						echo $s;
					}
					
				?>
			</p>
		</div>
	</div>
	<br>
	<div class="container">
		<div class="row">
			<?php
				$start = $pagination->config['limit'] * ($pagination->config['current_page'] - 1);
				$finish = min($start + $pagination->config['limit'],count($arr));
				// echo $start."--->".$finish."<br>";
				for ($i = $start; $i < $finish; $i++) { 
					$obj = $arr[$i];
			?>
					<div class="col-md-3 product" id="<?php echo $obj['id_product'] ?>">
						<img src="public/<?php echo $obj["link_img"];?>">
						<h3><?php echo $obj["brand"]; ?></h3>
						<p><?php echo $obj['name']; ?></p>
						<p><b><?php echo $obj['price'];  ?>đ</b></p>
					</div>
			<?php
				}
			?>	

		</div>
	</div>
	<div class="container">
		<div class="row">
			<?php echo $pagination->html(); ?>
		</div>
	</div>
	
	<script type="text/javascript" src="public/js/js1.js"></script>
</body>
</html>