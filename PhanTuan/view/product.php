<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">></script>
	<link rel="stylesheet" type="text/css" href="public/css/style2.css">

</head>
<body>
	<br><br>
	<div class="container">
		<div class="row" id="<?php echo $arr['id_product'] ?>">
			<div class="col-md-5">
				<img src="public/<?php echo $arr['link_img'] ?>" alt="Not found <?php echo $arr['link_img'] ?>">
			</div>
			<div class="col-md-7">
				<h3 style="color: green;"><?php echo $arr['brand'] ?></h3>
				<h2><?php echo $arr['name'] ?></h2>
				<br>
				Đặc điểm:
				<ul>
					<?php
						$string = array();
						$string[] = strtok($arr['function'], '.');
						while($s = strtok(".")) {
							$string[] = $s;
						}
						for($i=0; $i < count($string); $i++) {
							if($string[$i] != '' && $string[$i] != ' ') {
							?>
							<li><?php echo $string[$i] ?></li>
							<?php
							}
						}
					?>
				</ul>
				<p>Giá: <b><?php echo $arr['price'] ?>đ</b></p>
				<p>Xuất xứ: <?php echo $arr['made_in'] ?></p>
				<p>Khối lượng: <?php echo $arr['weight'] ?>g</p>
				<p>Số lượng: <?php echo $arr['quantity'] ?></p>
				<p>Đã bán: <?php echo $arr['sold_quantity'] ?></p>
				<br>
				<button id="edit">EDIT</button>
				<button id="delete">DELETE</button>				
				
			</div>
		</div>
	</div>
	<script type="text/javascript" src="public/js/js2.js"></script>
</body>
</html>