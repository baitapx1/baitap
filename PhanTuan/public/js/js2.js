$(window).ready(function () {

	var id = $('.row').attr('id');
	if(id == '') id = 0;

	$('#delete').click(function () {
		if(id == 0) return false;
		window.location = 'index.php?controller=product&status=delete&id=' + id;
	})

	$('#edit').click(function () {
		window.location = 'index.php?controller=update&id=' + id;
	})
	
})
