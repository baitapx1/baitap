$(window).ready(function () {
	
	$('#update').click(function () {
		var check = false;

		var link_img = $('input[type=file]').val();
		if(link_img == '') {
			alert("No File Choosen!!!");
			return false;
		}
		var type = [".jpg", ".png", ".gif", ".jpeg"];
		for (var i = 0; i < type.length; i++) {
			var pos = link_img.lastIndexOf(type[i]);
			if(link_img.length  == pos + type[i].length)
				check = true;
		}
		if(!check) {
			alert("Your file is not an image!!!");
			return false;
		}

		$('.number').each(function () {
			var a = $(this).val();
			if(!isInteger(a)) {
				$(this).css('background-color', 'lightpink');
				check = false;
			}
		});
		if(!check){
			alert("input is wrong!!! cannot convert String to Integer (>=0)");
		} 
		return check;
	})

})

function isInteger(str) {
	var s = "0123456789";
	for (var i = 0; i < str.length; i++) {
		if(s.indexOf(str[i]) < 0)
			return false;
	}
	return true;
}
