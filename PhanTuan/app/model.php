<?php 
	class model {
		public $conn;

		function __construct()
		{
			$this->conn = $GLOBALS['link'];
		}
		function selectAll($sql)
		{	
			$result = $this->conn->query($sql);
			$arr = array();
			while($row = $result->fetch_assoc()){
				$arr[] = $row;
			}
			return $arr;
		}
		function selectOne($sql)
		{	
			$result = $this->conn->query($sql);
			if($result->num_rows > 0) {
				$check = true;
				$arr = $result->fetch_assoc();
			} else $check = false;			

			$data = array();
			$data['id_product'] = $check ? $arr['id_product'] : "";
			$data['name'] = $check ? $arr['name'] : "";
			$data['brand'] = isset($arr['brand']) ? $arr['brand'] : "";
			$data['id_brand'] = $check ? $arr['id_brand'] : "";
			$data['category'] = $check ? $arr['category'] : "";
			$data['function'] = $check ? $arr['function'] : "";
			$data['link_img'] = $check ? $arr['link_img'] : "";
			$data['made_in'] = $check ? $arr['made_in'] : "";
			$data['weight'] = $check ? $arr['weight'] : "";
			$data['price'] = $check ? $arr['price'] : "";
			$data['quantity'] = $check ? $arr['quantity'] : "";
			$data['sold_quantity'] = $check ? $arr['sold_quantity'] : "";
			$data['function'] = str_replace("-", ".", $data['function']);
			$data['function'] = str_replace("..", ".", $data['function']);
			return $data;
		}
		function execute($sql)
		{
			$this->conn->query($sql);
		}
		function getNewestId() {
			return $this->conn->insert_id;
		}
	}
	
 ?>