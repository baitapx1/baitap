<?php
	class model {
		// lay nhieu ban ghi
		public function fetch($sql){
			global $con;
			$result = mysqli_query($con,$sql);
			$arr = array();
			while($row = mysqli_fetch_array($result)){
				$arr[] = $row;
			}
			return $arr;
		}
		// lay 1 ban ghi
		public function fetch_one($sql){
			global $con;
			$result = mysqli_query($con,$sql);
			$arr = mysqli_fetch_array($result);
			return $arr;
		}
		// dem so ban ghi
		public function count($sql){
			global $con;
			$result = mysqli_query($con,$sql);
			return mysqli_num_rows($result);
		}
		// set data or delete
		public function execute($sql){
			global $con;
			mysqli_query($con,$sql);
		}
	}
?>